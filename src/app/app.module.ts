import { router } from './site.roouter';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { CarouselComponent } from './carousel/carousel.component';
import { SelectSegmentoComponent } from './select-segmento/select-segmento.component';
import { BannerServicosComponent } from './banner-servicos/banner-servicos.component';
import { FooterComponent } from './footer/footer.component';
import { ContatoComponent } from './contato/contato.component';
import { InicialComponent } from './pages/inicial/inicial.component';
import {Routes,  RouterModule} from '@angular/router';
import { SegmentoComponent } from './pages/segmento/segmento.component';
import { CasesComponent } from './cases/cases.component';
import { PlanosComponent } from './planos/planos.component';
import { MenusService } from './_services/menus.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { SectionService } from './_services/sections.service';
import { EbooksComponent } from './pages/ebooks/ebooks.component';
import { ServicosComponent } from './pages/servicos/servicos.component';
import { BlogComponent } from './pages/blog/blog.component';
import { BlogService } from './_services/blog.service';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    CarouselComponent,
    SelectSegmentoComponent,
    BannerServicosComponent,
    FooterComponent,
    ContatoComponent,
    InicialComponent,
    SegmentoComponent,
    CasesComponent,
    PlanosComponent,
    EbooksComponent,
    ServicosComponent,
    BlogComponent

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    RouterModule,
    NgbModule.forRoot(),
    RouterModule.forRoot(router)
  ],
  providers: [MenusService, SectionService, BlogService],
  bootstrap: [AppComponent]
})
export class AppModule { }

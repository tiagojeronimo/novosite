import { Component, OnInit } from '@angular/core';
import { BlogService } from '../../_services/blog.service';
import { element } from 'protractor';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {

  constructor(private bs: BlogService) { }
  posts: any
  categorias: any
  listaPosts = []
  listaCategorias = []
  postsPage: any
  nrPosts = 3
  paginaAtual = 1
  paginaFim = 3
  mesesAno = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro']


  ngOnInit() {
    this.carregaPost()
    this.pagination(1)
    this.getCategories()

  }

  searchWords(event) {

    this.bs.searchPosts(event.srcElement.value).subscribe(
      res => {
        this.postsPage = res

        this.montaBlog(this.postsPage, false)
      },
      err => { }
    )

  }

  carregaPost() {
    this.bs.getAll().subscribe(
      res => {
        this.posts = res

      }, err => {
        console.log("erro ao tentar bustar os posts")
      }
    )
  }

  ultimaPagina: number
  pagination(page: number) {

    if (page < this.ultimaPagina || page == 1) {
      var remover = this.postsInPage.length - (page * 2)      
      this.postsInPage.splice(remover)
      if(page == 1){
        this.postsInPage = []
      }      
    }
    this.ultimaPagina = page

    this.paginaFim = (page + 3)

    this.bs.getPagination(this.nrPosts, page).subscribe(
      res => {
        this.postsPage = res
        this.montaBlog(this.postsPage, true)
      }, err => {
        console.log("teste")
      }
    )
  }

  getCategories() {
    this.bs.getCategories().subscribe(
      res => {
        this.categorias = res
      }
    )
  }

  postsInPage = []
  montaBlog(postagens, pagination: boolean) {
    if (postagens) {

      this.listaPosts = []
      console.log("PostsInPage in montaBlog")
      console.log(this.postsInPage)

      this.postsPage.forEach(element => {

        console.log("element id")
        console.log(element.id)

        if (element.id != 84 && this.postsInPage.indexOf(element.id) == -1) {

          this.postsInPage.push(element.id)

          this.listaPosts.push({
            "id": element.id,
            "titulo": element['title'].rendered,
            "conteudo": element['content'].rendered,
            "data_criacao": element.modified_gmt,
            "author": element.author,
            "tag": element.tags
          })
        }
      });

      if (this.listaPosts) {
        this.insereConteudoDiv()
      }
    }
  }

  private insereConteudoDiv() {
    this.listaPosts.forEach(post => {
      setTimeout(() => {
        var arrData = post.data_criacao.split("-")

        var getDia = arrData[2].split("T")
        var diaPost = getDia[0]
        var dataCriacao = diaPost + ' de ' + this.mesesAno[parseInt(arrData[1]) - 1] + ' de ' + arrData[0]

        this.getNomeAutor(parseInt(post.author), post.id)

        if (post.tag) {
          this.getTag(parseInt(post.tag), post.id)
        }
        this.countComents(post.id)

        document.getElementById("contextoArtigo_" + post.id).innerHTML = post.conteudo

        document.getElementById("dataPost_" + post.id).innerHTML = dataCriacao

      }, 500)

    });
  }

  getNomeAutor(id: number, idDiv) {
    this.bs.getAuthor(id).subscribe(
      res => {
        setTimeout(() => {
          document.getElementById("autorPost_" + parseInt(idDiv)).innerHTML = 'Por ' + res['name'] + ' | '
        }, 500)
      }, err => {
        console.log("Nome do Autor não encontrado")
      }
    )
  }


  getTag(id, idDiv) {
    console.log("ID da TAG")
    console.log()
    if (!isNaN(id)) {
      this.bs.getTag(id).subscribe(
        res => {
          setTimeout(() => {
            document.getElementById("flagPost_" + parseInt(idDiv)).innerHTML = res['name']
          }, 500)
        }, err => {

        }
      )
    }
  }


  countComents(postId) {
    var comentarios: any
    var arrComent = []

    this.bs.getComentarios().subscribe(
      res => {
        comentarios = res
        setTimeout(() => {
          comentarios.forEach(element => {
            if (element['post'] == postId) {
              arrComent.push(element['id'])
            }
          });

          document.getElementById("nrComentarios_" + postId).innerHTML = " | Comentários " + arrComent.length

        }, 500)
      },
      err => {

      }
    )
  }


}

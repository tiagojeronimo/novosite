import { Component, OnInit } from '@angular/core';
import { MenusService } from '../../_services/menus.service';
import { element } from 'protractor';

@Component({
  selector: 'app-ebooks',
  templateUrl: './ebooks.component.html',
  styleUrls: ['./ebooks.component.css']
})
export class EbooksComponent implements OnInit {

  constructor(private ms: MenusService) { }
  ebooks: any
  preencherDados = false
  podeBaixar = false
  ngOnInit() {
    this.getEbooksDown()
  }


  getEbooksDown() {
    this.ms.getMedia().subscribe(
      res => {
        this.ebooks = res
        if (this.ebooks) {
          this.getEbooks(this.ebooks)
        }
      },
      err => {
        console.log("erro ao procurar imagens")
      }
    )
  }

  downEbooks = []
  getEbooks(ebooks) {
    if (ebooks) {
      var idxI = 0
      this.ebooks.forEach(element => {
        if (element.link.indexOf('ebook') > -1) {
          this.downEbooks.push({ "url": element.source_url, "id": idxI, "titulo": element.caption.rendered, "descricao": element.description.rendered })
        }
        idxI++
      });

      if (this.downEbooks) {
        this.downEbooks.forEach(element => {
          setTimeout(() => {
            document.getElementById("tituloEbook_" + element.id).innerHTML = element.titulo
            document.getElementById("descricao_" + element.id).innerHTML = element.descricao
          }, 500)

        })
      }

    }
  }


  baixarEbook(){
    this.preencherDados = true
  }

  enviarDadosEbook(){
    this.podeBaixar = true
    this.downEbooks = []
    this.getEbooksDown()
  }

}

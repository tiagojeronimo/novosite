import { Component, OnInit } from '@angular/core';
import { SectionService } from '../../_services/sections.service';

@Component({
  selector: 'app-segmento',
  templateUrl: './segmento.component.html',
  styleUrls: ['./segmento.component.css']
})
export class SegmentoComponent implements OnInit {

  htmlApi: any;
  sectionHtml: any;
  constructor(private sec: SectionService) { }

  ngOnInit() {

    this.verificaRota()
  }


  insertHtmlSection(idSegmento:number){
    this.sectionHtml = document.getElementById("conteudoSegmento")
    this.sec.getSection(idSegmento).subscribe(
      res => {        
        this.htmlApi = res["content"].rendered
        this.sectionHtml.innerHTML =  res["content"].rendered
      }, err => {

      }
    )
  }


  verificaRota(){
    var rota = window.location.href

    if(rota.indexOf('farmacia') > -1){
      this.insertHtmlSection(61)
    }else if(rota.indexOf('conveniencia') > -1){
      this.insertHtmlSection(75)
    }else if (rota.indexOf('ferragem') > -1){
      this.insertHtmlSection(72)
    }else if(rota.indexOf('loja') > -1){
      this.insertHtmlSection(78)
    }else{
      console.log("rota não encontrada")
    }


  }

}

import { Component, OnInit } from '@angular/core';
import { SectionService } from '../_services/sections.service';

@Component({
  selector: 'select-segmento',
  templateUrl: './select-segmento.component.html',
  styleUrls: ['./select-segmento.component.css']
})
export class SelectSegmentoComponent implements OnInit {

  htmlApi: any;
  segmentos: any[];
  constructor(private sec: SectionService) { }
  sectionHtml:any
  titulo:string
  subtitulo:string
  
  
  ngOnInit() {

    this.insertHtmlSection()
  }

  insertHtmlSection(){
    this.sectionHtml = document.getElementById("sectionSegmentos")
    this.sec.getSection(43).subscribe(
      res => {
        
        this.htmlApi = res["content"].rendered

        this.sectionHtml.innerHTML =  res["content"].rendered
      }, err => {

      }
    )
  }
    



}

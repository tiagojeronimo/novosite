import { SegmentoComponent } from './pages/segmento/segmento.component';
import { InicialComponent } from './pages/inicial/inicial.component';
import {Routes} from '@angular/router';
import { EbooksComponent } from './pages/ebooks/ebooks.component';
import { ServicosComponent } from './pages/servicos/servicos.component';
import { BlogComponent } from './pages/blog/blog.component';

export const router: Routes = [
  {
    path: '',
    component: InicialComponent
  },
  {
    path: 'Home',
    component: InicialComponent
  }, 
  {
    path: 'servicos',
    component: ServicosComponent
  },  
  {
    path: 'blog',
    component: BlogComponent
  },     
  {
    path: 'ebooks',
    component: EbooksComponent
  },
  {
    path: 'segmento/:seg',
    component: SegmentoComponent
  }  

];

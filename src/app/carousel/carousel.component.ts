import { Component, OnInit } from '@angular/core';
import {NgbCarouselConfig} from '@ng-bootstrap/ng-bootstrap';
import { MenusService } from '../_services/menus.service';
@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css']
})
export class CarouselComponent implements OnInit {

  constructor(config: NgbCarouselConfig, private ms: MenusService) {
    config.interval = 10000;
    config.wrap = false;
    config.keyboard = false;

   }

  carousel:any
  ngOnInit() {
    this.getCarouselImages()
  }

  getCarouselImages(){
    this.ms.getMedia().subscribe(
      res => {

        this.carousel = res
        if(this.carousel){
          this.getImagesCarousel(this.carousel)
        }
      },
      err => {
        console.log("erro ao procurar imagens")
      }
    )    
  }

  linksCarousel = []
   
  getImagesCarousel(carousel){
    if(carousel){
      var idxI = 0
       this.carousel.forEach(element => {
        if(element.link.indexOf('carousel') != -1){          
          this.linksCarousel.push({"url": element.source_url, "id": idxI})
        }
        idxI++
     });
     console.log(this.carousel)
    }   
  }
}

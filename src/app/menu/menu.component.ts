import { Component, OnInit } from '@angular/core';
import { MenusService } from '../_services/menus.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  constructor(private ms: MenusService) { }
  
  menu:any
 
  ngOnInit() {
    this.ms.getMenu('top').subscribe(menu => this.menu = menu)


  }
  abreMenu:boolean

  verificaSeExisteClass(){

    
    if(this.abreMenu){
      var element = document.getElementById("navbarSupportedContent");
      element.classList.add("collapse");
      this.abreMenu = false
    }else{
      var element = document.getElementById("navbarSupportedContent");
      element.classList.remove("collapse"); 
      this.abreMenu = true   
    }


  }

}

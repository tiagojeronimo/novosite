import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {SITE_API} from '../site.api'

@Injectable()
export class BlogService {

  constructor(private http: HttpClient) { }
  
  getAll(){
    return this.http.get(SITE_API + 'wp/v2/posts');
  }
  getPagination(nrPost:number, page:number){
    return this.http.get(SITE_API + 'wp/v2/posts?per_page=' + nrPost + '&page=' + page)
  }

  getPost(id:number){
    return this.http.get(SITE_API + 'wp/v2/posts/' + id);
  }
  
  getCategories(){
    return this.http.get(SITE_API + 'wp/v2/categories/');
  }

  getAuthor(id:number){
    return this.http.get(SITE_API + 'wp/v2/users/' + id); 
  }

  getTag(id:number){
    return this.http.get(SITE_API + 'wp/v2/tags/' + id); 
  }

getComentarios(){
  return this.http.get(SITE_API + 'wp/v2/comments/');
}

searchPosts(palavras:string){

  var url = 'wp/v2/search/' + palavras

  return this.http.get(SITE_API + url)
}

}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {SITE_API} from '../site.api'

@Injectable()
export class SectionService {

  constructor(private http: HttpClient) { }

  getSection(id:number){
    return this.http.get(SITE_API + 'wp/v2/pages/' + id);
  }
  

}

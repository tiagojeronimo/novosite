import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {SITE_API} from '../site.api'

@Injectable()
export class MenusService {

  constructor(private http: HttpClient) { }

  getMenu(menu:string){
    return this.http.get(SITE_API + 'menus/v1/menus/primary');
  }
  
  getMedia(){
    return this.http.get(SITE_API + 'wp/v2/media')
  }

}

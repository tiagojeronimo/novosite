import { Component } from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import { router } from './site.roouter';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'Site Stringhini';
  currentRoute = '';
  menus: Array<Object> = [];
}
